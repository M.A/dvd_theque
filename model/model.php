<?php

class Films extends PDO{

    private $_auteur;
    private $_annee;
    private $_titre;
    private $_genre;
    
    /**
     * Constructeur du parent PDO pour la co DB
     */
    public function __construct() {
        parent::__construct('mysql:dbname=dvdtheque;host=localhost', 'root', '');
    }

    public function getAuteur($auteur) {

        return $this->_auteur;
    }

    public function getAnnee($annee) {

        return $this->_annee;
    }

    public function getTitre($titre) {

        return $this->_titre;
    }

    public function getGenre($genre) {

        return $this->_genre;
    }

    /**
     * Check si annee est un int
     * @param type $annee
     * @return type
     */
    public function setAnnee($annee) {

        if (is_int($annee)) {
            return $annee;
        } else {
            trigger_error('Erreur', E_USER_WARNING);
            return;
        }
    }

    /**
     * Verif si genre est un integer
     * @param type $genre
     * @return type
     */
    public function setGenre($genre) {

        if (is_int($genre)) {
            trigger_error('Erreur', E_USER_WARNING);
            return;
        } else {
            return $genre;
        }
    }

    /**
     * Visualisation de la liste des films
     * @return type
     */
    public function viewFilms() {
        $sth = $this->prepare('SELECT * FROM films');
        $sth->execute();
        $req = $sth->fetchAll();
        return $req;
    }

    /**
     * Ajout d'un film
     * @param type $titre
     * @param type $genre
     * @param type $auteur
     * @param type $annee
     */
    public function ajoutFilms($titre, $setGenre, $auteur, $setAnnee) {
        $sth = $this->prepare('INSERT INTO films (auteur, annee, titre, genre) VALUES (:auteur, :annee, :titre, :genre)');
        $sth->execute(array('auteur' => $auteur, 'annee' => $setAnnee, 'titre' => $titre, 'genre' => $setGenre));
    }

    /**
     * Placeholder edit current film
     * @param type $id
     * @return type
     */
    public function premodiFilms($id) {
        $sth = $this->prepare('SELECT * FROM films where id = :id');
        $sth->execute(array('id' => $id));
        $req = $sth->fetchAll();
        return $req;
    }

    /**
     * Modification d'un film
     * @param type $titre
     * @param type $genre
     * @param type $auteur
     * @param type $annee
     */
    public function modiFilms($titre, $genre, $auteur, $annee, $id) {
        $sth = $this->prepare('UPDATE films SET auteur = :auteur, annee = :annee, titre = :titre, genre = :genre WHERE id = :id');
        $sth->execute(array('auteur' => $auteur, 'annee' => $annee, 'titre' => $titre, 'genre' => $genre, 'id' => $id));
    }

    /**
     * Effacement d'un film
     * @param type $deleteId
     */
    public function deleteFilms($deleteId) {
        $sth = $this->prepare('DELETE FROM films WHERE id =  :filmID');
        $sth->execute(array('filmID' => $deleteId));
    }

}
