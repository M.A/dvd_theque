<?php

include '../model/model.php';
include '../view/functions.php';

//Ajout film
if (isset($_POST['titre']) && isset($_POST['genre']) && isset($_POST['auteur']) && isset($_POST['annee'])) {

    $membre = new Films();

    $titre = $_POST['titre'];
    $genre = $_POST['genre'];
    $auteur = $_POST['auteur'];
    $annee = (int)$_POST['annee'];

    $setAnnee = $membre->setAnnee($annee);
    $setGenre = $membre->setGenre($genre);
    $membre->ajoutFilms($titre, $setGenre, $auteur, $setAnnee);
    safe_redirect('../');
}

//Modif film
if (isset($_POST['Modiftitre']) && isset($_POST['Modifgenre']) && isset($_POST['Modifauteur']) && isset($_POST['Modifannee']) && isset($_POST['hiddenId'])) {

    $membre = new Films();

    $titre = $_POST['Modiftitre'];
    $genre = $_POST['Modifgenre'];
    $auteur = $_POST['Modifauteur'];
    $annee = $_POST['Modifannee'];
    $id = $_POST['hiddenId'];

    $membre->modiFilms($titre, $genre, $auteur, $annee, $id);
    safe_redirect('../');
}

//View modif
if (isset($_GET['modif'])) {
    $newModif = new Films();
    
    $id = $_GET['modif'];
    $films = $newModif->premodiFilms($id);
    
    include_once '../view/modif.php';
}

//Suppresion film
if (isset($_GET['delete'])) {

    $membre = new Films();
    $deleteId = $_GET['delete'];

    $membre->deleteFilms($deleteId);

    safe_redirect('../');
}

//Sinon voir films
else {

    $newFilm = new Films();
    $films = $newFilm->viewFilms();

    include_once '../view/viewFilms.php';
}