<html>
    <head>
        <meta charset="UTF-8">
        <!-- CSS  -->
        <link rel="stylesheet" type="text/css" href="../view/css/bootstrap.css">
    </head>
    <body>

        <form method="post" name="filmInteract">


            <h1>DVDtheque</h1>

            <h3>Mes dvds</h3>

            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>Titre</th>
                        <th>Genre</th>
                        <th>Auteur</th>
                        <th>Année de production</th>
                        <th>Modifier</th>
                        <th></th>
                    </tr>
                </thead>

                <tbody>
                    <?php
                    if (count($films) == 0) {
                        echo "<h4>Vous n avez aucun film </h4>";
                    } else {
                        foreach ($films as $film) {
                            ?> 
                            <tr>
                        <form method="post">
                            <td><?php echo $film['titre']; ?></td>
                            <td><?php echo $film['genre']; ?></td>
                            <td><?php echo $film['auteur']; ?></td>
                            <td><?php echo $film['annee']; ?></td>
                            <td><a href="?modif=<?php echo $film['id']; ?>" class="btn btn-primary" >Modifier</a></td>
                            <td><a href="?delete=<?php echo $film['id']; ?>">X</a></td>
                        </form>    
                        </tr>
                        <?php
                    }
                }
                ?> 
                <form method="post">
                    <tr>
                        <td><input type="text" name="titre" required="required"></td>
                        <td><input type="text" name="genre" required="required"></td>
                        <td><input type="text" name="auteur" required="required"></td>
                        <td><input type="number" min="1900" max="<?php echo date("Y"); ?>" name="annee" required="required"></td>
                        <td></td>
                        <td><input type="submit" class="btn btn-primary pull-right" name="Ajouter" value="Ajouter"></td>
                    </tr>
                </form>
                </tbody>

            </table>
        </form>
    </body> 
</html>      


