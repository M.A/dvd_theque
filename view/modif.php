<html>
    <head>
        <meta charset="UTF-8">
        <!-- CSS  -->
        <link rel="stylesheet" type="text/css" href="../view/css/bootstrap.css">
    </head>
    <body>
        <h3>Edit current film</h3>
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>Titre</th>
                    <th>Genre</th>
                    <th>Auteur</th>
                    <th>Année de production</th>
                    <th>Modifier</th>
                </tr>
            </thead>

            <tbody>
                <tr>
                    <?php
                    foreach ($films as $film) {
                        ?> 
                <form method="post">
                    <td><input type="text" name="Modiftitre" placeholder="<?php echo $film['titre']; ?>"></input></td>
                    <td><input type="text" name="Modifgenre" placeholder="<?php echo $film['genre']; ?>"></input></td>
                    <td><input type="text" name="Modifauteur" placeholder="<?php echo $film['auteur']; ?>"></input></td>
                    <td><input type="number" min="1900" max="<?php echo date("Y"); ?>" name="Modifannee" placeholder="<?php echo $film['annee']; ?>"></input></td>
                    <input type="hidden" name="hiddenId" value="<?php echo $film['id']; ?>"> </input>
                    <td><input type="submit" class="btn btn-primary pull-right" name="Modifier" value="Modifier"></td>
                </form>    
                <?php
            }
            ?> 
        </tr>
    </tbody>
</table>
<input type="button" value="Retour" class="btn btn-primary" onclick="history.go(-1)">
</body>